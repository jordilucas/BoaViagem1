package com.br.boaviagem;

import java.util.Calendar;

import android.os.Bundle;
import android.app.*;
import android.app.DatePickerDialog.OnDateSetListener;
import android.view.*;
import android.widget.*;

public class GastoActivity extends Activity {
	
	private int ano,mes,dia	;
	private Button dataGasto;
	Spinner categoria;
	String[] vetor = new String[5];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gasto);
		
		Calendar calendar = Calendar.getInstance();
		ano = calendar.get(Calendar.YEAR);
		mes = calendar.get(Calendar.MONTH);
		dia = calendar.get(Calendar.DAY_OF_MONTH);
		dataGasto = (Button)findViewById(R.id.data);
		dataGasto.setText(dia +"/"+ (mes+1) +"/"+ ano);
		for(int i=0;i<5;i++){
			vetor[0] = String.valueOf("Alimentação");
			vetor[1] = String.valueOf("Combustivel");
			vetor[2] = String.valueOf("Transporte");
			vetor[3] = String.valueOf("Hospedagem");
			vetor[4] = String.valueOf("Outros");
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,vetor);
		categoria = (Spinner)findViewById(R.id.categoria);
		categoria.setAdapter(adapter);
	}
	
	public void selecionarData(View view){
		showDialog(view.getId());
	}
	@Override
	protected Dialog onCreateDialog(int id){
		if(R.id.data == id){
			return new DatePickerDialog(this, listener,ano,mes,dia);
		}
		return null;
	}
	private OnDateSetListener listener = new OnDateSetListener(){
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
		ano = year;
		mes = monthOfYear;
		dia = dayOfMonth;
		dataGasto.setText(dia +"/"+ (mes+1) +"/"+ ano);
		}
	};
	
	

}
