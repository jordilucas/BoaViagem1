package com.br.boaviagem;

import java.util.Calendar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DatePickerDialog.OnDateSetListener;


public class ViagemActivity extends Activity {

	private int ano,mes,dia	;
	private Button dataGasto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cadastro_viagem);
		
		Calendar calendar = Calendar.getInstance();
		ano = calendar.get(Calendar.YEAR);
		mes = calendar.get(Calendar.MONTH);
		dia = calendar.get(Calendar.DAY_OF_MONTH);
		dataGasto = (Button)findViewById(R.id.data);
		dataGasto.setText(dia +"/"+ (mes+1) +"/"+ ano);
	
	}
	public void selecionarData(View view){
		showDialog(view.getId());
	}
	@Override
	protected Dialog onCreateDialog(int id){
		if(R.id.data == id){
			return new DatePickerDialog(this, listener,ano,mes,dia);
		}
		return null;
	}
	private OnDateSetListener listener = new OnDateSetListener(){
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
		ano = year;
		mes = monthOfYear;
		dia = dayOfMonth;
		dataGasto.setText(dia +"/"+ (mes+1) +"/"+ ano);
		}
	};
	
	
	
	
}
