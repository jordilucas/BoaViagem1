package br.com.boaviagem;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.*;
import android.widget.*;

import com.br.boaviagem.R;

import br.com.boaviagem.DashboardActivity;

public class LoginActivity extends Activity {
	private EditText usuario;
	private EditText senha;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		usuario = (EditText) findViewById(R.id.usuario);
		senha = (EditText) findViewById(R.id.senha);

	}
	
	

	
	
	public void entrarOnClick(View v){

		String usuarioinformado = usuario.getText().toString();
		String senhainformada = senha.getText().toString();
		
		if("jordi".equals(usuarioinformado)&& "123".equals(senhainformada)){
			startActivity(new Intent(this,DashboardActivity.class));
		}
		
		
		else{
			String mensagemErro = getString(R.string.erro_autenticacao);
			Toast toast = Toast.makeText(this, mensagemErro, Toast.LENGTH_SHORT);
			toast.show();
			usuario.setText("");
			senha.setText("");
		}
		
	}

}
