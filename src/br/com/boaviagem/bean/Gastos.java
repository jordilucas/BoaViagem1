package br.com.boaviagem.bean;

/**
 * Created by jordi on 24/04/14.
 */
public class Gastos {

    private String categoria;
    private double valor;
    private int data;
    private String descricao;
    private String local;

    public void setCategoria(String cat){
        categoria = cat;
    }

    public String getCategoria(){
        return categoria;
    }

    public void setValor(double valor){
        this.valor = valor;
    }

    public double getValor(){
        return valor;
    }

    public void setData(int data){
        this.data = data;
    }

    public int getData(){
        return data;
    }

    public void setDescricao(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao(){
        return descricao;
    }

    public void setLocal(String local){
        this.local = local;
    }

    public String getLocal(){
        return local;
    }





}
