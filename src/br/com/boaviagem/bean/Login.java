package br.com.boaviagem.bean;

/**
 * Created by jordi on 24/04/14.
 */
public class Login {

    private String usuario;
    private String senha;

    public void setUsuario(String user){
        usuario = user;
    }

    public String getUsuario(){
        return usuario;
    }

    public void setSenha(String senha){
        this.senha = senha;
    }

    public String getSenha(){
        return senha;
    }

}
