package br.com.boaviagem.bean;

/**
 * Created by jordi on 24/04/14.
 */
public class Viagem {

    private String novoDestino;
    private String tipoViagem;
    private int dtChegada;
    private int dtSaida;
    private double orcamento;
    private int qtdPessoas;


    public void setNovoDestino(String destino){
        novoDestino = destino;
    }
    public String getNovoDestino(){
        return novoDestino;
    }

    public void setTipoViagem(String viagem){
        tipoViagem = viagem;
    }
    public String getTipoViagem(){
        return tipoViagem;
    }

    public void setDtChegada(int dt_chegada){
        dtChegada = dt_chegada;
    }

    public int getDtChegada(){
        return dtChegada;
    }

    public void setDtSaida(int dt_saida){
        dtSaida = dt_saida;
    }

    public int getDtSaida(){
        return dtSaida;
    }

    public void setOrcamento(double valor){
        orcamento = valor;
    }

    public double getOrcamento(){
        return orcamento;
    }

    public void setQtdPessoas(int pessoas){
        qtdPessoas = pessoas;
    }

    public int getQtdPessoas(){
        return qtdPessoas;
    }


}
