package br.com.boaviagem.helper;

import android.app.Activity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.br.boaviagem.R;

import br.com.boaviagem.GastoActivity;
import br.com.boaviagem.bean.Gastos;

/**
 * Created by jordi on 24/04/14.
 */
public class Gastohelper {

    private Spinner categoria;
    private EditText valor;
    private Button data;
    private EditText descricao;
    private EditText local;

    private Gastos gasto;

    public Gastohelper(GastoActivity activity){

        categoria = (Spinner)activity.findViewById(R.id.categoria);
        valor     = (EditText)activity.findViewById(R.id.valor);
        data      = (Button)activity.findViewById(R.id.data);
        descricao = (EditText)activity.findViewById(R.id.descricao);
        local     = (EditText)activity.findViewById(R.id.local);

        gasto     = new Gastos();

    }

    public Gastos getGasto(){

        gasto.setCategoria(categoria.getSelectedItem().toString());
        gasto.setValor(Integer.parseInt(valor.getText().toString()));
        gasto.setData(Integer.parseInt(data.getText().toString()));
        gasto.setDescricao(descricao.getText().toString());
        gasto.setLocal(local.getText().toString());

        return gasto;

    }



}
