package br.com.boaviagem.helper;

import android.widget.EditText;

import com.br.boaviagem.R;

import br.com.boaviagem.LoginActivity;
import br.com.boaviagem.bean.Login;

/**
 * Created by jordi on 24/04/14.
 */
public class loginHelper {
    private EditText nome;
    private EditText senha;

    private Login login = new Login();

    public loginHelper(LoginActivity activity){

        nome = (EditText)activity.findViewById(R.id.usuario);
        senha = (EditText)activity.findViewById(R.id.senha);
    }

    public Login getLogin(){
        login.setUsuario(nome.getText().toString());
        login.setSenha(senha.getText().toString());

        return login;
    }



}
